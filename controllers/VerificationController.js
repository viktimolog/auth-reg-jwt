const Uuid = require('uuidv4');
const VerificationKey = require('../models/VerificationKey');
const sendMail = require('../utils/mail');
const appConfig = require('../config/app');
const errorCreator = require('../utils/error');
const responses = require('../utils/responses');

class VerificationController {
    async confirmVerification(request, response, next) {
        const { verificationKey } = request.body;

        const userVerificationKey = await VerificationKey.findOne({ hash: verificationKey });
        if (!userVerificationKey) {
            const error = new Error('verification: verificationKey was not found in database')
            error.httpStatusCode = 500;
            console.log(error);
            const payload = await errorCreator(401, 'Sorry, this verification key is wrong!');
            return responses.errorResponse(response, payload);
        }

        if (!userVerificationKey.verifiedAt) {
            userVerificationKey.verifiedAt = Date.now();
            const isVerificationKeyDbSave = await userVerificationKey.save();
            if (!isVerificationKeyDbSave) {
                const error = new Error('verification: verificationKey was not saved in database')
                error.httpStatusCode = 500;
                return next(error)
            }
        }

        const payload = {
            message: 'Your email was verified!'
        };
        return responses.successResponse(response, payload);
    }

    async resendLetter(request, response, next) {
        const { user } = request;

        const oldUserVerificationKey = await VerificationKey.findOne({ userId: user._id });
        if (!oldUserVerificationKey) {
            const error = new Error('resendLetter: oldUserVerificationKey was not found in database')
            error.httpStatusCode = 500;
            next(error)
        }

        const removeOldUserVerificationKey = await oldUserVerificationKey.remove();
        if (!removeOldUserVerificationKey) {
            const error = new Error('resendLetter: old verificationKey was not deleted from database')
            error.httpStatusCode = 500;
            return next(error)
        }

        const uuid = await Uuid();
        if (!uuid) {
            const error = new Error('resendLetter: uuid was not created');
            error.httpStatusCode = 500;
            return next(error);
        }

        const verKey = await new VerificationKey({ userId: user._id, hash: uuid }).save();
        if (!verKey) {
            const error = new Error('resendLetter: hash was not added to database');
            error.httpStatusCode = 500;
            return next(error);
        }

        const path = appConfig.baseUrl + `email-verification/${uuid}`;
        if (!path) {
            const error = new Error('resendLetter: path was not created');
            error.httpStatusCode = 500;
            console.log(error);
        }

        //send mail to user
        const isSendMail = await sendMail(path, user.email,
            'Email confirmation from auth-req-jwt', 'Follow the link to verify your email: ');
        if (!isSendMail) {
            const error = new Error('resendLetter: a verification letter was not sent');
            error.httpStatusCode = 500;
            console.log(error);
            const payload = await errorCreator(422, 'Sorry, a new verification letter was not sent');
            return responses.errorResponse(response, payload);
        }

        const payload = {
            message: 'We have sent you a new verification letter. Please, follow the instructions in it.'
        };
        return responses.successResponse(response, payload);
    }
}

module.exports = new VerificationController();