const bcrypt = require('bcryptjs');
const Joi = require('joi');
const Uuid = require('uuidv4');

const User = require('../models/User');
const RecoveryPasswordKey = require('../models/RecoveryPasswordKey');
const VerificationKey = require('../models/VerificationKey');
const userSchema = require('../validation/userSchema');
const emailSchema = require('../validation/emailSchema');
const passwordSchema = require('../validation/passwordSchema');
const appConfig = require('../config/app');
const generateHash = require('../utils/hash');
const sendMail = require('../utils/mail');
const createToken = require('../utils/token');
const error = require('../utils/error');
const responses = require('../utils/responses');

class AccountController {
    //Forgot password
    async forgotPassword(request, response, next) {
        const { email } = request.body;

        const emailNotValidated = Joi.validate(request.body, emailSchema, error => error);
        if (emailNotValidated) {
            const payload = await error(422, emailNotValidated.details[0].message);
            return responses.errorResponse(response, payload);
        }

        // Find user by email
        const user = await User.findOne({ email });
        if (!user) {
            const payload = await error(401, 'Access denied');
            return responses.errorResponse(response, payload);
        }

        const uuid = await Uuid();
        if (!uuid) {
            const error = new Error('forgotPassword: uuid was not created');
            error.httpStatusCode = 500;
            return next(error);
        }

        const newRecoveryPasswordKey = await new RecoveryPasswordKey({ userId: user._id, hash: uuid }).save();
        if (!newRecoveryPasswordKey) {
            const error = new Error('forgotPassword: newRecoveryPasswordKey was not added to database')
            error.httpStatusCode = 500;
            return next(error);
        }

        const path = appConfig.baseUrl + `restore-password/${uuid}`;
        if (!path) {
            const error = new Error('forgotPassword: path was not created');
            error.httpStatusCode = 500;
            console.log(error);
        }

        //send mail to user
        const isSendMail = await sendMail(path, email,
            'Password recovery from auth-req-jwt', 'Follow the link to recovery your password: ');
        if (!isSendMail) {
            const error = new Error('forgotPassword: email with path for password recovery was not sent');
            error.httpStatusCode = 500;
            console.log(error);
        }

        const payload = {
            message: 'We have sent you a new letter. Please, follow the instructions in it.'
        };
        return responses.successResponse(response, payload);
    }

    async checkRecoveryPasswordKey(request, response) {
        const { recoveryPasswordKey: recoveryPasswordKeyRequest } = request.body;

        // Find RecoveryPasswordKey by recoveryPasswordKeyRequest
        const recoveryPasswordKey = await RecoveryPasswordKey.findOne({ hash: recoveryPasswordKeyRequest });
        if (!recoveryPasswordKey || !!recoveryPasswordKey.usedAt) {
            const payload = await error(401, 'Sorry, this key for password recovery is wrong!');
            return responses.errorResponse(response, payload);
        }
        return responses.successResponse(response);
    }

    async restorePassword(request, response, next) {
        const { password, recoveryPasswordKey: recoveryPasswordKeyRequest } = request.body;

        // Find RecoveryPasswordKey by recoveryPasswordKeyRequest
        const recoveryPasswordKey = await RecoveryPasswordKey.findOne({ hash: recoveryPasswordKeyRequest });
        if (!recoveryPasswordKey || !!recoveryPasswordKey.usedAt) {
            const payload = await error(401, 'Sorry, this key for password recovery is wrong!');
            return responses.errorResponse(response, payload);
        }

        const passwordNotValidated = Joi.validate({ password }, passwordSchema, error => error);

        if (passwordNotValidated) {
            const payload = await error(422, passwordNotValidated.details[0].message);
            return responses.errorResponse(response, payload);
        }

        // Find user by id
        const user = await User.findOne({ _id: recoveryPasswordKey.userId });
        if (!user) {
            const payload = await error(401, 'Access denied');
            return responses.errorResponse(response, payload);
        }

        const hashPassword = await generateHash(password);
        if (!hashPassword) {
            const error = new Error('restorePassword: hashPassword was not generated');
            error.httpStatusCode = 500;
            return next(error);
        }

        user.password = hashPassword;

        const editedUser = await user.save();
        if (!editedUser) {
            const error = new Error('restorePassword: editUser was not saved to database');
            error.httpStatusCode = 500;
            return next(error);
        }

        recoveryPasswordKey.usedAt = Date.now();
        const isRecoveryPasswordKeyDbSave = await recoveryPasswordKey.save();
        if (!isRecoveryPasswordKeyDbSave) {
            const error = new Error('restorePassword: recoveryPasswordKey was not saved in database')
            error.httpStatusCode = 500;
            return next(error)
        }

        const payload = {
            message: 'Your password was edited!'
        };
        return responses.successResponse(response, payload);
    }

    //Register
    async register(request, response, next) {
        const { email, password } = request.body;

        const userNotValidated = Joi.validate(request.body, userSchema, error => error);

        if (userNotValidated) {
            const payload = await error(422, userNotValidated.details[0].message);
            return responses.errorResponse(response, payload);
        }

        const user = await User.findOne({ email });

        if (user) {
            const payload = await error(422, `Sorry, email: ${email} has already been taken!`);
            return responses.errorResponse(response, payload);
        }

        const hashPassword = await generateHash(password);
        if (!hashPassword) {
            const error = new Error('register: hashPassword was not generated');
            error.httpStatusCode = 500;
            return next(error);
        }

        const newUser = await new User({ email, password: hashPassword }).save();

        if (!newUser) {
            const error = new Error('register: newUser was not added to database');
            error.httpStatusCode = 500;
            return next(error);
        }

        const uuid = await Uuid();
        if (!uuid) {
            const error = new Error('register: uuid was not created');
            error.httpStatusCode = 500;
            return next(error);
        }

        const verKey = await new VerificationKey({ userId: newUser._id, hash: uuid }).save();
        if (!verKey) {
            const error = new Error('register: hash was not added to database');
            error.httpStatusCode = 500;
            return next(error);
        }

        const path = appConfig.baseUrl + `email-verification/${uuid}`;
        if (!path) {
            const error = new Error('register: path was not created');
            error.httpStatusCode = 500;
            console.log(error);
        }

        //send mail to user
        const isSendMail = await sendMail(path, email,
            'Email confirmation from auth-req-jwt', 'Follow the link to verify your email: ');
        if (!isSendMail) {
            const error = new Error('register: a verification letter was not sent');
            error.httpStatusCode = 500;
            console.log(error);
        }

        const payload = await createToken(newUser);
        if (!payload) {
            const error = new Error('register: payload was not created');
            error.httpStatusCode = 500;
            return next(error);
        }
        return responses.successResponse(response, payload);
    }

    //Log in
    async login(request, response, next) {
        const { email, password } = request.body;

        const userNotValidated = Joi.validate(request.body, userSchema, error => error);

        if (userNotValidated) {
            const payload = await error(401, 'Access denied');
            return responses.errorResponse(response, payload);
        }

        // Find user by email
        const user = await User.findOne({ email });
        if (!user) {
            const payload = await error(401, 'Access denied');
            return responses.errorResponse(response, payload);
        }

        // Check Password
        const passIsMatch = await bcrypt.compare(password, user.password);
        if (!passIsMatch) {
            const payload = await error(422, 'Email or password is incorrect');
            return responses.errorResponse(response, payload);
        }

        const payload = await createToken(user);
        if (!payload) {
            const error = new Error('login: payload was not created');
            error.httpStatusCode = 500;
            return next(error)
        }
        return responses.successResponse(response, payload);
    }

    //get Account
    async getAccount(request, response, next) {
        const { user } = request;
        const userVerificationKey = await VerificationKey.findOne({ userId: user._id });

        if (!userVerificationKey) {
            const error = new Error('getAccount: verificationKey was not found in database')
            error.httpStatusCode = 500;
            return next(error)
        }

        const account = {
            userId: user._id,
            email: user.email,
            isVerified: !!userVerificationKey.verifiedAt
        };

        const payload = {
            account
        };
        return responses.successResponse(response, payload);
    }
}

module.exports = new AccountController();