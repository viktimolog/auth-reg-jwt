require('dotenv').config();
const express = require('express')
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParcer = require('body-parser');
const passport = require('passport');

const users = require('./routes/users');
const db = require('./config/db');

const app = express();

// Body parcer middleware
app.use(bodyParcer.urlencoded({ extended: false }));
app.use(bodyParcer.json());

// DB config
const mongoConnect = `mongodb://${db.mongoUser}:${db.mongoPass}@${db.mongoURL}`;

// Connect to MongoDB
mongoose
    .connect(mongoConnect, { useNewUrlParser: true })
    .then(() => console.log('MongoDB Connected'))
    .catch(err => console.log(err));

// Passport Config
require('./config/passport')(passport);

// Passport middleware
app.use(passport.initialize());

app.use(cors());

app.get('/', (req, res) => res
    .send('This my 1st task from ReZet: auth, reg, jwt'))

// Use Routes
app.use('/users', users);

app.use(function (err, req, res, next) {
    console.error('err.stack = ', err.stack);
    res.status(500).send(err.stack);
});

app.listen(process.env.PORT, () => console.log(`Listening on ${process.env.PORT}`))
