const router = require('express-promise-router')();
const accountController = require('../controllers/AccountController');
const verificationController = require('../controllers/VerificationController');
const guards = require('../middlewares/guards');

router.post('/login', accountController.login);
router.post('/register', accountController.register);
router.post('/restorePassword', accountController.restorePassword);
router.post('/forgotPassword', accountController.forgotPassword);
router.post('/checkRecoveryPasswordKey', accountController.checkRecoveryPasswordKey);
router.post('/verify', verificationController.confirmVerification);

router.post('/account', guards.jwt(), accountController.getAccount);
router.post('/resendLetter', guards.jwt(), verificationController.resendLetter);

module.exports = router;

