const Joi = require('joi');

module.exports = Joi.object().keys({
    password: Joi.string().alphanum().min(3).max(30).required(),
    email: Joi.string().email().required()
});