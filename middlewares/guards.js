const passport = require('passport');
const responses = require('../utils/responses');

module.exports = {
    jwt: () => (request, response, next) => {
        passport.authenticate('jwt', { session: false },
            (error, user) => {
                if (error) {
                    return responses.errorResponse(response);
                } else {
                    request.user = user;
                    return next();
                }
            })(request, response, next);
    }
};