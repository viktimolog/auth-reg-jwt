const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create Schema
const VerificationKeySchema = new Schema({
    userId: {
        type: String,
        required: true
    },
    hash: {
        type: String,
        required: true
    },
    verifiedAt: {
        type: Date,
        default: null
    }
});

module.exports = mongoose.model('verificationKeys', VerificationKeySchema);
