const error = function (code, message) {
    const payload = {
        errors: [{
            code,
            message
        }]
    }
    return payload;
};
module.exports = error;