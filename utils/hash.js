const bcrypt = require('bcryptjs');

const generateHash = async function (str) {
    const hash = await bcrypt.hash(str, await bcrypt.genSalt(10));
    return hash;
};
module.exports = generateHash;
