const jwt = require('jsonwebtoken');
const _ = require('lodash');
const VerificationKey = require('../models/VerificationKey');
const appCredentials = require('../config/app');

const createToken = async function (user) {
    const account = {};
    account.userId = user._id;
    account.email = user.email;

    const dataToken = _.pick(user, ['_id', 'email']);
    // Create JWT Payload
    // Sign Token
    const jwtToken = await jwt.sign(dataToken, appCredentials.secretOrKey, { expiresIn: 360000 });

    if (!jwtToken) {
        return false;
    }

    const payload = {
        account
    };

    payload.token = jwtToken;

    const userVerificationKey = await VerificationKey.findOne({ userId: user._id });
    if (!userVerificationKey) {
        return false;
    }

    account.isVerified = !!userVerificationKey.verifiedAt;
    return payload;
};

module.exports = createToken;