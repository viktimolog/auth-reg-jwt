const nodeMailer = require('nodemailer');
const appConfig = require('../config/app');

const sendMail = async function (path = '', to, subject, html) {
    const transport = nodeMailer.createTransport({
        host: 'smtp.gmail.com',
        port: 25,
        secure: false,
        auth: {
            user: appConfig.senderEmail,
            pass: appConfig.senderPassword
        },
        tls: {
            rejectUnauthorized: false
        }
    });
    const mailOptions = {
        from: '"auth-req-jwt" <auth-req-jwt@gmail.com>', // sender address
        to, // list of receivers
        subject: `${subject}`, // Subject line
        html: `${html}${path}` // html body
    };

    const res = await transport.sendMail(mailOptions)
        .then(info => true)
        .catch(err => false);
    return res;
};

module.exports = sendMail;