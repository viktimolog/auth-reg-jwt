module.exports = {
    successResponse: function (response, payload = null) {
        return response.json({ success: true, payload });
    },

    errorResponse: function (response, payload = null) {
        return response.json({ success: false, payload });
    }
}