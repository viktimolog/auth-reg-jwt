require('dotenv').config();
module.exports = {
    senderEmail: process.env.senderEmail || 'testtestovnyj@gmail.com',
    senderPassword: process.env.senderPassword || 'testtestovnyjpassword',
    baseUrl: process.env.baseUrl || 'http://localhost:3000/',
    secretOrKey: process.env.secretOrKey || 'secret'
};